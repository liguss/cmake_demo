# 构建说明
1、安装环境
- 构建管理工具：cmake 3.22.1
- 构建工具：make （当前使用MinGW32-make）
- 编译链：gcc/g++ （当前使用MinGW32下的gcc/g++）
2、构建说明
- windows环境下生成makefile需要指令 cmake .. -G "Unix Makefiles" 否则无法生成
- 需要配置指定的C/C++编译工具路径
- 如果出现二进制文件找不到什么的，可能是你忘记加“..”来指定实际的CMakeLists.txt文件的目录了，如果编译仍报错，需要先手动清除一下上次的cmake错误编译记录

# 文件结构
├── CMakeLists.txt
├── subbinary
│   ├── CMakeLists.txt
│   └── main.cpp
├── sublibrary1
│   ├── CMakeLists.txt
│   ├── include
│   │   └── sublib1
│   │       └── sublib1.h
│   └── src
│       └── sublib1.cpp
└── sublibrary2
    ├── CMakeLists.txt
    └── include
        └── sublib2
            └── sublib2.h